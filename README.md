# Nest

# Libraries Used

- nestjs/common : contains vast majority of functions, classes, etc, that we need from Nest
- nestjs/platform_express: Lets Nest use Express JS for handling HTTP requests
- reflect-metadata: Helps make decorators work.
- typescript: instead of JS we use Typescript because its awesome.

# How Nest server works

- Nest server uses HTTP implementation from libraries like Express or Fastify via adapter (nestjs/ platform_express for express)
- Pipe: is used to validate data contained in the request
- Guard: For usage of authentication
- Controller: Route the request to a particular function
- Service: Run some business logic
- Repository: Access a database

## Basic commit rules

Use the following guidelines for commit

feat: A new feature for the user. (add login functionality)

fix: A bug fix for the user. (resolve issue with user authentication)

chore: Routine tasks, maintenance, or tooling changes. (update dependencies)

docs: Changes to documentation. (update installation instructions)

style: Code style changes (e.g., formatting). (format code according to style guide)

refactor: Code changes that neither fix a bug nor add a feature. (simplify user validation logic)

test: Adding or modifying tests. (add unit tests for user service)

# For detailed documentation

https://wary-shield-e2a.notion.site/NestJS-138ead7040574d1097571ed159923820?pvs=4
