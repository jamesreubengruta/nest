import { Module } from '@nestjs/common';
import { AwsController } from './aws.controller';
import { AwsDynamoDBHelper } from './aws.dynamodbhelper';
import { AwsService } from './aws.service';
@Module({
  exports: [AwsDynamoDBHelper],
  controllers: [AwsController],
  providers: [AwsService, AwsDynamoDBHelper],
})
export class AwsModule {}
