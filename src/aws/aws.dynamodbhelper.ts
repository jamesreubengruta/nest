import { Injectable } from '@nestjs/common';
import * as AWS from 'aws-sdk';
import { DocumentClient } from 'aws-sdk/clients/dynamodb';
import 'dotenv/config';

const { AWS_REGION, AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY } = process.env;

@Injectable()
export class AwsDynamoDBHelper {
  constructor() {
    AWS.config.update({
      accessKeyId: AWS_ACCESS_KEY_ID,
      secretAccessKey: AWS_SECRET_ACCESS_KEY,
      region: AWS_REGION,
    });
  }

  dynamoDBClient = (): DocumentClient => {
    return new AWS.DynamoDB.DocumentClient();
  };

  async dynamoDBHelperQuery(params: DocumentClient.QueryInput): Promise<any[]> {
    const results = await this.dynamoDBClient().query(params).promise();

    return results.Items;
  }
}
