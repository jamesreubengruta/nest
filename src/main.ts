import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
//import { MessagesModule } from './messages/messages.module';
import { CoreModule } from './core/core.module';
async function bootstrap() {
  const app = await NestFactory.create(CoreModule);
  app.useGlobalPipes(new ValidationPipe());
  await app.listen(3000);
}
bootstrap();
