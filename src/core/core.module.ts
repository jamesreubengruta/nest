import { Module } from '@nestjs/common';
import { AwsModule } from 'src/aws/aws.module';
import { MessagesModule } from 'src/messages/messages.module';
import { NotesModule } from 'src/notes/notes.module';
import { CoreController } from './core.controller';
import { CoreService } from './core.service';

@Module({
  imports: [MessagesModule, NotesModule, AwsModule],
  controllers: [CoreController],
  providers: [CoreService],
})
export class CoreModule {}
