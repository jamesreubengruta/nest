import { Injectable } from '@nestjs/common';
import { createHash } from 'node:crypto';
import { AwsDynamoDBHelper } from 'src/aws/aws.dynamodbhelper';
import { v4 as uuid } from 'uuid';
import { CreateCredentialsDTO, CreateProfileDTO } from './dtos/notes.dto';
const profiles_table = 'Profiles';
//const note_table = 'Notes';
const credentials_table = 'Credentials';
@Injectable()
export class NotesRepository {
  constructor(public dynamo: AwsDynamoDBHelper) {}

  // async findOne(profileId: string): Promise<any> {
  //   const params = {
  //     TableName: 'ProfileTable',
  //     Key: {
  //       profile_id: profileId,
  //     },
  //   };

  //   return this.dynamoDB.get(params).promise();
  // }

  async findOne(id: string) {
    const params = {
      TableName: profiles_table,
      KeyConditionExpression: 'profile_id = :profile_id',
      ExpressionAttributeValues: {
        ':profile_id': id,
      },
    };

    const items = await this.dynamo.dynamoDBHelperQuery(params);

    return items;
  }

  async findAll() {
    const results = await this.dynamo
      .dynamoDBClient()
      .scan({
        TableName: profiles_table,
      })
      .promise();

    return results.Items;
  }

  async create(dto: CreateProfileDTO) {
    try {
      //uses basic put for single transaction
      await this.dynamo
        .dynamoDBClient()
        .put({
          TableName: profiles_table,
          Item: {
            profile_id: uuid(),
            first_name: dto.first_name,
            last_name: dto.last_name,
            email_address: dto.email_address,
            contact_no: dto.contact_no,
          },
        })
        .promise();
      return {
        message: 'Data successfully added!',
      };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  async registration(
    profile_dto: CreateProfileDTO,
    credentials_dto: CreateCredentialsDTO,
  ) {
    const profile_id = uuid();
    try {
      const credential_id = uuid();
      const hashedPassword = createHash('sha256')
        .update(credentials_dto.password)
        .digest('hex');

      // Use DynamoDB transactions for atomicity
      await this.dynamo
        .dynamoDBClient()
        .transactWrite({
          TransactItems: [
            {
              Put: {
                TableName: credentials_table,
                Item: {
                  credential_id: credential_id,
                  profile_id: profile_id,
                  password: hashedPassword,
                },
              },
            },
            {
              Put: {
                TableName: profiles_table,
                Item: {
                  profile_id: profile_id,
                  first_name: profile_dto.first_name,
                  last_name: profile_dto.last_name,
                  email_address: profile_dto.email_address,
                  contact_no: profile_dto.contact_no,
                },
              },
            },
          ],
        })
        .promise();

      return {
        message: 'Data successfully added!',
      };
    } catch (error) {
      console.log(error, error.CancellationReasons);
      return error;
    }
  }

  async login(email: string, password: string): Promise<boolean> {
    try {
      // Step 1: Query the profiles table to get the profile_id associated with the email
      const profileItem: AWS.DynamoDB.DocumentClient.GetItemOutput =
        await this.dynamo
          .dynamoDBClient()
          .get({
            TableName: 'profiles_table',
            Key: {
              email_address: email,
            },
          })
          .promise();

      if (!profileItem || !profileItem.Item?.profile_id) {
        // No profile found with the given email
        return false;
      }

      const profileId = profileItem.Item.profile_id;

      // Step 2: Query the credentials table to check if the password matches
      const hashedPassword = createHash('sha256')
        .update(password)
        .digest('hex');
      const credentialsItem = await this.dynamo
        .dynamoDBClient()
        .get({
          TableName: 'credentials_table',
          Key: {
            profile_id: profileId,
            credential_id: profileId, // Assuming profileId is used as credential_id as well
          },
        })
        .promise();

      if (
        !credentialsItem ||
        credentialsItem.Item?.password !== hashedPassword
      ) {
        // Passwords do not match
        return false;
      }

      // Authentication successful
      return true;
    } catch (error) {
      // Log and handle the error appropriately
      console.error('Error during authentication:', error);
      return false;
    }
  }
}
