import { Injectable } from '@nestjs/common';
import { CreateCredentialsDTO, CreateProfileDTO } from './dtos/notes.dto';
import { NotesRepository } from './notes.repository';

@Injectable()
export class NotesService {
  constructor(public repo: NotesRepository) {}

  async findOne(id: string) {
    return this.repo.findOne(id);
  }

  async findAll() {
    return this.repo.findAll();
  }

  async createProfile(data: CreateProfileDTO) {
    return this.repo.create(data);
  }

  async registration(profile: CreateProfileDTO, creds: CreateCredentialsDTO) {
    return this.repo.registration(profile, creds);
  }
}
