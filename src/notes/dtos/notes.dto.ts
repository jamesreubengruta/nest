import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class CreateProfileDTO {
  @IsNotEmpty()
  contact_no: string;
  @IsNotEmpty()
  @IsString()
  last_name: string;
  @IsNotEmpty()
  @IsString()
  @IsEmail()
  email_address: string;
  @IsNotEmpty()
  @IsString()
  first_name: string;
}

export class CreateCredentialsDTO {
  @IsNotEmpty()
  @IsString()
  password: string;
}
