import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { CreateCredentialsDTO, CreateProfileDTO } from './dtos/notes.dto';
import { NotesService } from './notes.service';
@Controller('notes')
export class NotesController {
  constructor(public services: NotesService) {}

  @Get()
  listProfile() {
    return this.services.findAll();
  }

  @Get('/:id')
  async getProfile(@Param('id') id: string) {
    return this.services.findOne(id);
  }

  @Post('/createProfile')
  async createProfile(@Body() data: CreateProfileDTO) {
    return this.services.createProfile(data);
  }

  @Post('/register')
  async register(
    @Body() profile: CreateProfileDTO,
    @Body() creds: CreateCredentialsDTO,
  ) {
    return this.services.registration(profile, creds);
  }
}

//@Get('/:id')
// async getMessage(@Param('id') id: string) {
