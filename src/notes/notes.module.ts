import { Module } from '@nestjs/common';
import { AwsModule } from 'src/aws/aws.module';
import { NotesController } from './notes.controller';
import { NotesRepository } from './notes.repository';
import { NotesService } from './notes.service';
@Module({
  controllers: [NotesController],
  providers: [NotesService, NotesRepository],
  exports: [NotesService],
  imports: [AwsModule],
})
export class NotesModule {}
