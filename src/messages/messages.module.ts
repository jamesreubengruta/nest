import { Module } from '@nestjs/common';
import { MessagesController } from './messages.controller';
import { MessagesRepository } from './messages.repository';
import { MessagesServices } from './messages.services';
@Module({
  imports: [], //import other modules here
  controllers: [MessagesController],
  providers: [MessagesServices, MessagesRepository],
  exports: [MessagesServices],
})
export class MessagesModule {}
