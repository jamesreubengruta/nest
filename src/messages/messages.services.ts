import { Injectable } from '@nestjs/common';
import { MessagesRepository } from './messages.repository';
@Injectable()
export class MessagesServices {
  constructor(public repo: MessagesRepository) {}

  async findOne(id: string) {
    return this.repo.findOne(id);
  }

  async findAll() {
    return this.repo.findAll();
  }

  async createMessage(data: string) {
    return this.repo.createData(data);
  }
}
