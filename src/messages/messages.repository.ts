import { Injectable } from '@nestjs/common';
import { readFile, writeFile } from 'fs/promises';
@Injectable()
export class MessagesRepository {
  async findOne(id: string) {
    const data = await readFile('messages.json', 'utf-8');
    const message = JSON.parse(data);
    return message[id];
  }

  async findAll() {
    const data = await readFile('messages.json', 'utf-8');
    const message = JSON.parse(data);
    return message;
  }

  async createData(content: string) {
    const data = await readFile('messages.json', 'utf-8');
    const message = JSON.parse(data);
    const id = Math.floor(Math.random() * 999);
    message[id] = { id, content };
    writeFile('messages.json', JSON.stringify(message));
    return content;
  }
}
