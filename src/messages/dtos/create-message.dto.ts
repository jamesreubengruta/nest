import { IsAlphanumeric, IsNotEmpty, IsString } from 'class-validator';

export class CreateMessageDto {
  @IsNotEmpty()
  @IsString()
  content: string;
}

export class CreatePasswordDto {
  @IsNotEmpty()
  @IsString()
  @IsAlphanumeric()
  password: string;
}
