import {
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Post,
} from '@nestjs/common';
import { CreateMessageDto, CreatePasswordDto } from './dtos/create-message.dto';
import { MessagesServices } from './messages.services';
@Controller('messages')
export class MessagesController {
  constructor(public services: MessagesServices) {}

  @Get()
  listMessages() {
    return this.services.findAll();
  }

  @Post()
  createMessage(@Body() body: CreateMessageDto) {
    console.log(body);
    return this.services.createMessage(body.content);
  }
  @Post('/createPassword')
  createPassword(@Body() body: CreatePasswordDto) {
    console.log(body);
    return this.services.createMessage(body.password);
  }
  @Get('/:id')
  async getMessage(@Param('id') id: string) {
    const data = await this.services.findOne(id);

    if (!data) {
      throw new NotFoundException('404 Not found');
    } else {
      return data;
    }
  }
}
